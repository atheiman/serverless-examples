# Serverless Examples

The goal here is to provide example serverless framework services ...

- in javascript and python
- with both API gateway integration methods, `Lambda-Proxy` and `Lambda` (sometimes called `Lambda-Custom`)
- with event debugging (dump the event passed to the lambda function)
- with error raising (trigger a 400 HTTP error)
- with gitlab ci testing and deployment

## Resources

- https://serverless.com/learn/quick-start/
- https://serverless.com/blog/flask-python-rest-api-serverless-lambda-dynamodb/
- https://serverless.com/blog/serverless-express-rest-api/
- https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-with-lambda-integration.html
- https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
- https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-lambda-non-proxy-integration.html
- https://medium.com/@lakshmanLD/lambda-proxy-vs-lambda-integration-in-aws-api-gateway-3a9397af0e6d
