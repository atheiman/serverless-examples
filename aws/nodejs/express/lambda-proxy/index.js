const serverless = require('serverless-http');
const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json({ strict: false }));

app.all('/', function (req, res) {
  res.send({
    // https://expressjs.com/en/4x/api.html#req
    app: req.app,
    baseUrl: req.baseUrl,
    body: req.body,
    cookies: req.cookies,
    fresh: req.fresh,
    hostname: req.hostname,
    ip: req.ip,
    ips: req.ips,
    method: req.method,
    originalUrl: req.originalUrl,
    params: req.params,
    path: req.path,
    protocol: req.protocol,
    query: req.query,
    route: req.route,
    secure: req.secure,
    signedCookies: req.signedCookies,
    stale: req.stale,
    subdomains: req.subdomains,
    url: req.url,
    xhr: req.xhr
  });
});

app.get('/error', function (req, res) {
  res.status(400).send({ error: 'You sent an error!' });
});

module.exports.handler = serverless(app);
