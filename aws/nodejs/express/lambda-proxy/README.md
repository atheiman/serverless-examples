```shell
# deploy the service
sls deploy -v
# grab the "ServiceEndpoint" from the end of the output

curl -H content-type:application/json -d '{"json": "body"}' '<ServiceEndpoint>/?query=string' | python -m json.tool

# destroy the service
sls remove -v
```
