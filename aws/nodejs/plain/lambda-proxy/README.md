```shell
# deploy the service
sls deploy -v
# grab the "ServiceEndpoint" from the end of the output

curl -d '{"json": "body"}' '<ServiceEndpoint>/pathOne/pathTwo?query=string' | python -m json.tool

# destroy the service
sls remove -v
```
