'use strict';

module.exports.dumpEvent = (event, context, callback) => {
  // attempt to parse body as JSON
  try {
    event.body = JSON.parse(event.body);
  } catch (e) {}

  const response = {
    statusCode: 200,
    body: JSON.stringify({ event: event }),
  };

  callback(null, response);
};
