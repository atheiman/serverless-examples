import json

def dump_handler(event, context):
    event['body'] = json.loads(event['body'])

    body = {
        "event": event
    }

    return {
        "statusCode": 200,
        "body": json.dumps(body)
    }

def error_handler(event, context):
    try:
        raise Exception('Custom error message about a bad request from the client')
    except Exception as err:
        return {
            "statusCode": 400,
            "body": json.dumps({ "error": str(err) })
        }
