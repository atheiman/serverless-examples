```shell
# deploy the service
sls deploy -v
# grab the "ServiceEndpoint" from the end of the output

curl -d '{"json": "body"}' '<ServiceEndpoint>/arg1/arg2/arg3/arg4?query' | python -m json.tool

# destroy the service
sls remove -v
```
